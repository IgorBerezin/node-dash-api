module.exports = {
	Client: require('./client'),
	Locations: require('./locations'),
	Phones: require('./phones')
};