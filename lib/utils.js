/**
 * Method to join all URI parts together
 * @param {String[]} arr Array of uri parts
 * @returns {String} Combined URI
 */
module.exports.joinURL = (arr) => {
	if (!arr || arr.length <= 0) {
		return '';
	}

	let normalized = [];
	let query = '';

	for (let i = 0; i < arr.length; ++i) {
		var part = arr[i];

		if (typeof part !== 'string') {
			if (typeof part !== 'number') {
				throw new TypeError('URI parts must be an array of strings or numbers.');
			}
			part = part.toString();
		}

		// Removal of starting and ending slashes for every part except the first one.
		part = part.replace(/^\s*\/*\s*|\s*\/*\s*$/gm, '').trim();
		if (part) {
			if (part[0] === '?') {
				query += part;
			} else {
				if (query) {
					query += part;
				} else {
					normalized.push(part);
				}
			}
		}
	}

	return normalized.join('/') + query;
};


/**
 * Stringify unicode
 * @param {Object} o input
 * @returns {String} unicode string representation of input
 */
module.exports.stringifyUnicode = (o) => {
	//next time look over: https://github.com/mathiasbynens/jsesc

	return JSON.stringify(o).replace(/[\u007f-\uffff]/g, (c) => {
		return '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4);
	});
};

/**
 * Convert input date into Local ISO Asp format if possible
 * @param {Object} d Datetime value
 * @returns {String} Local ISO Asp or input
 */
module.exports.convertDateToLocalISOAspString = (d) => {
	const pad = (number) => {
		if (number < 10) {
			return '0' + number;
		}
		return number;
	};

	if (d instanceof Date) {
		return d.getFullYear() +
			'-' + pad(d.getMonth() + 1) +
			'-' + pad(d.getDate()) +
			'T' + pad(d.getHours()) +
			':' + pad(d.getMinutes()) +
			':' + pad(d.getSeconds());
	} else {
		return d;
	}
};

/**
 * Get JSON representation of SetDateFieldSettings
 * @summary
 * SetDateFieldSettings is a string representation of a special JSON object.
 *
 * SetActiveDateSettings:
 * { "DesiredActiveDate": {String with Asp Date}, "SetDateBehavior": {String with SetDateBehavior} }
 *
 * SetDisconnectDateSettings:
 * {
 *   "DesiredDisconnectDate": {String with Asp Date},
 *   "DisconnectReason": {String},
 *   "SetDateBehavior": {String with SetDateBehavior}
 * }
 *
 * ClearDisconnectDateSettings:
 * { "CurrentDisconnectDate": {String with Asp Date} }
 *
 * SetDateBehavior must be one of
 *    Apply-To-Items-Without-A-Current-Date
 *    Apply-To-All-Items
 *    Apply-To-Items-Without-A-Current-Date-Only-These-Details-Care-Provisioning-LD-Free
 *    Apply-To-Items-Without-A-Current-Date-No-Details
 *    Apply-To-Items-With-Date-yyyyMMdd
 * @param {String} template Action tempate (equal to workflow Action)
 * @param {String} date ASP date of action
 * @param {String} note Action note/reason
 * @param {String} behavior Date behavior
 * @returns {String} JSON string of settings
 */
module.exports.getSetDateFieldSettings = (template, date, note, behavior) => {
	let value = {};
	if (template === 'Activate') {
		value = {
			DesiredActiveDate: date,
			SetDateBehavior: behavior||'Apply-To-Items-Without-A-Current-Date'
		};
	} else if (template === 'Disconnect') {
		value = {
			DesiredDisconnectDate: date,
			DisconnectReason: note||'',
			SetDateBehavior: behavior ||'Apply-To-Items-Without-A-Current-Date'
		};
	} else if (template === 'Reconnect') {
		value = {
			CurrentDisconnectDate: date
		};
	}

	return JSON.stringify(value);
};

/**
 * Get query for list requests
 * @param {Object} options list options
 * @param {Object} defaults client default values
 * @returns {String} url query for list endpoint
 */
module.exports.getListQuery = (options, defaults) => {
	let query = '';
	const addQuery = (key, val) => {
		if (!query) {
			query += '?';
		} else {
			query += '&';
		}
		query += key + '=' + val;
	};

	const pageSize = (options && options.pageSize) ? options.pageSize : defaults.pageSize;

	if (pageSize) {
		addQuery('Take', pageSize);
		if (pageSize !== 'All' && options && options.pageIndex > 1) {
			addQuery('Skip', options.pageSize * (options.pageIndex - 1));
		}
	}

	if (options.filter) {
		addQuery('Filter', options.filter);
	}

	return query;
};