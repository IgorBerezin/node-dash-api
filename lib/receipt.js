const xml2js = require('xml2js');
const parser = new xml2js.Parser(/* options */);

/**
 * Main receipt package
 * Represents HTTP execution recipt.
 * @return {Receipt}
 */
const Receipt = module.exports = class Receipt {
	/**
	 * Construct a new Receipt
	 *
	 * @constructor
	 * @param {Object} request Request info
	 * @param {String} title Client title
	 */
	constructor(request, title) {
		this._request = request;
		this._start = new Date();
		this._error = null;
		this._end = null;
		this._responseBody = null;
		this._title = title || '';
	}

	/**
	 * Http request
	 */
	get Request() {
		return this._request;
	}

	/**
	 * Distination title
	 */
	get Title() {
		return this._title;
	}

	/**
	 * Request started time
	 */
	get Start() {
		return this._start;
	}
	/**
	 * Response error getter
	 */
	get Error() {
		return this._error;
	}

	/**
	 * Response getter
	 */
	get Response() {
		return this._response;
	}

	/**
	 * Response body getter
	 */
	get ResponseBody() {
		return this._responseBody;
	}

	/**
	 * Response object
	 */
	get ResponseObject() {
		return this._responseObject;
	}

	/**
	 * Response finished time
	 */
	get End() {
		return this._end;
	}

	/**
	 * HTTP request duration
	 */
	get Duration() {
		if (!this._end) {
			return 0;
		}
		return this._end - this._start;
	}

	/**
	 * Primary data for loggers
	 */
	get LogData() {
		return (this._error instanceof Error) ? this._error.message : this._error || this._responseObject;
	}


	/**
	 * Receipt convertation into raw object.
	 */
	ToRawObject() {
		return {
			duration: this.Duration,
			endpoint: this.Request.baseUrl + this.Request.url,
			method: this.Request.method,
			headers: this.Request.headers,
			request: this.Request.body,
			error: this.Error,
			response: this.LogData
		};
	}

	applyCB(err, response, body, cb) {
		this.apply(err, response, body)
			.then(() => cb())
			.catch((err) => cb(err));
	}

	/**
	 * Applying request result
	 * @param {Error} err Error
	 * @param {Object} response Response data
	 * @param {Object} body Response body
	 */
	async apply(err, response, body) {
		this._end = new Date();

		if (err) {
			this._error = err;
		}
		// console.log('response', body);

		this._response = response || { statusCode: 503, statusMessage: 'Service Unavailable' }; //Service Unavailable for unknown answers

		if (this._response.statusCode !== 200 && this._response.statusCode !== 201) {
			await this.wrapErrorBody(this._response.statusMessage, body);
		} else {
			await this.wrapBody(body);
		}
	}

	/**
	 * Wrapping response object
	 * @param {String} body Response body
	 */
	async wrapBody(body) {
		let self = this;

		if (body) {
			this._responseBody = body;
		}

		if (self.Request.headers['Content-Type'] === 'text/html') {
			self._responseObject = (Buffer.isBuffer(body)) ? body : new Buffer(body);
		} else {
			try {
				self._responseObject = await parser.parseStringPromise(body);
			} catch (e) {
				self._error = new Error(self.Title + ': Wrong response.');
			}
		}
	}

	/**
	 * Wrapping response Error
	 * @param {String} statusMessage Response status message
	 * @param {String} body Response body
	 */
	async wrapErrorBody(statusMessage, body) {
		let self = this;
		const defaultErrorMessage = self.Title + ' encountered an error processing the request.';

		if (body) {
			this._responseBody = body;
		}

		try {
			let jsonBody = await parser.parseStringPromise(body);


			if (jsonBody.XMLFault && jsonBody.XMLFault.faultstring) {
				self._error = new Error(jsonBody.XMLFault.faultstring);
			} else {
				self._error = new Error(defaultErrorMessage);
			}
		} catch (e) {
			if (statusMessage && statusMessage.indexOf('<?xml') === 0) {
				self._error = new Error(defaultErrorMessage);
				self._error.name = 'Request error';
			} else {
				self._error = new Error(statusMessage || self.Title + ': Wrong response.');
			}
		}
	}

};
