const Client = require('./client');
const utils = require('./utils');
/**
 * Main Phones package
 * Represents functionality to request Phones API.
 * @extends {Client}
 * @return {Phones}
 */
const Phones = module.exports = class Phones extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'uris';
	}

	/**
	 * Retrieving a collection of objects
	 * @returns {Receipt}
	 */
	async list() {
		return await super.execGetRequest(this.Api);
		/*
<ns2:getURIsResponse xmlns:ns2="http://dashcs.com/api/v1/emergency">
	<URIs>
		<uris>
			<callername>Caller One</callername>
			<uri>tel:19195551234</uri>
		</uris>
		<uris>
			<callername>Caller Two</callername>
			<uri>tel:19195554321</uri>
		</uris>
		<uris>
			<callername>Caller Three</callername>
			<uri>tel:19195559876</uri>
		</uris>
	</URIs>
</ns2:getURIsResponse>
		*/
	}

	/**
	 * Find all the locations associated with the given URI.
	 * @param {Number} id Phone number (URI)
	 * @returns {Receipt}
	 */
	async locations(id) {
		if (isNaN(id)) {
			throw new Error('Wrong telephone number.');
		}

		return await super.execGetRequest(utils.joinURL(['locationsbyuri', id]));
		/*
<ns0:getLocationsByURIResponse xmlns:ns0="http://dashcs.com/api/v1/emergency" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<Locations>
		<activatedtime>2014-10-08T16:57:48Z</activatedtime>
		<address1>1855 BLAKE ST</address1>
		<address2 xsi:nil="true" />
		<callername>Bandwidth Test User 1</callername>
		<comments xsi:nil="true" />
		<community>DENVER</community>
		<customerorderid xsi:nil="true" />
		<latitude>39.75302</latitude>
		<legacydata>
			<housenumber>1855</housenumber>
			<predirectional xsi:nil="true" />
			<streetname>BLAKE ST</streetname>
			<suite xsi:nil="true" />
		</legacydata>
		<locationid>20914831</locationid>
		<longitude>-104.996203</longitude>
		<plusfour>1288</plusfour>
		<postalcode>80202</postalcode>
		<state>CO</state>
		<status>
			<code>PROVISIONED</code>
			<description>Location is currently provisioned for URI</description>
		</status>
		<type>ADDRESS</type>
		<updatetime>2014-10-08T17:23:49Z</updatetime>
	</Locations>
	<Locations>
		<activatedtime>2014-10-08T16:57:48Z</activatedtime>
		<address1>2850 ST LAWERENCE AVE</address1>
		<address2 xsi:nil="true" />
		<callername>Bandwidth Test User 1</callername>
		<comments xsi:nil="true" />
		<community>READING</community>
		<customerorderid xsi:nil="true" />
		<latitude>40.331245</latitude>
		<legacydata>
			<housenumber>2850</housenumber>
			<predirectional xsi:nil="true" />
			<streetname>ST LAWERENCE AVE</streetname>
			<suite xsi:nil="true" />
		</legacydata>
		<locationid>21018169</locationid>
		<longitude>-75.849602</longitude>
		<plusfour xsi:nil="true" />
		<postalcode>19606</postalcode>
		<state>PA</state>
		<status>
			<code>GEOCODED</code>
			<description>Location is geocoded</description>
		</status>
		<type>ADDRESS</type>
		<updatetime>2015-04-08T23:45:07Z</updatetime>
	</Locations>
</ns0:getLocationsByURIResponse>
		*/
	}


	/**
	 * Get the provisioned location associated with a URI.
	 * You can have multiple locations associated with a URI but only one can be provisioned at a time.
	 * @param {Number} id Phone number (URI)
	 * @returns {Receipt}
	 */
	async provisioned(id) {
		if (isNaN(id)) {
			throw new Error('Wrong telephone number.');
		}

		return await super.execGetRequest(utils.joinURL(['provisionedlocationbyuri', id]));
		/*
<ns0:getLocationsByURIResponse xmlns:ns0="http://dashcs.com/api/v1/emergency" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<Location>
		<activatedtime>2014-10-08T16:57:48Z</activatedtime>
		<address1>1855 BLAKE ST</address1>
		<address2 xsi:nil="true" />
		<callername>Bandwidth Test User 1</callername>
		<comments xsi:nil="true" />
		<community>DENVER</community>
		<customerorderid xsi:nil="true" />
		<latitude>39.75302</latitude>
		<legacydata>
			<housenumber>1855</housenumber>
			<predirectional xsi:nil="true" />
			<streetname>BLAKE ST</streetname>
			<suite xsi:nil="true" />
		</legacydata>
		<locationid>20914831</locationid>
		<longitude>-104.996203</longitude>
		<plusfour>1288</plusfour>
		<postalcode>80202</postalcode>
		<state>CO</state>
		<status>
			<code>PROVISIONED</code>
			<description>Location is currently provisioned for URI</description>
		</status>
		<type>ADDRESS</type>
		<updatetime>2014-10-08T17:23:49Z</updatetime>
	</Location>
</ns0:getLocationsByURIResponse>
		*/
	}

	/**
	 * Remove a URI. Also remove all locations that were associated with the URI
	 * @param {Number} id Phone Number (URI)
	 * @returns {Receipt}
	 */
	async delete(id) {
		if (isNaN(id)) {
			throw new Error('Wrong telephone number.');
		}

		let xml =
			`<removeUri>
			<uri>${id}</uri>
		</removeUri>`;
		return await super.execPostRequest(xml, 'removeuri');
		/*
		<ns0:removeURIResponse xmlns:ns0="http://dashcs.com/api/v1/emergency">
			<URIStatus>
				<code>REMOVED</code>
				<description>URI removed from system</description>
			</URIStatus>
		</ns0:removeURIResponse>
		*/
	}
};