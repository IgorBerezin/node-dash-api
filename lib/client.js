const request = require('request');

const utils = require('./utils');
const Receipt = require('./receipt');

/**
 * Main client package
 * Represents Client settings and proxy methods invoke PTT.
 * @return {Client}
 */
const Client = module.exports = class Client {
	/**
	 * Construct a new Client
	 *
	 * @constructor
	 * @param {Object} config Config
	 * @param {Object} config.authorization Authorization
	 * @param {Object} config.authorization.type Auth type (Basic)
	 * @param {String} config.authorization.username PTT login
	 * @param {String} config.authorization.password PTT password
	 * @param {Object} config.url Uniform Resource Locator
	 * @param {String} config.url.host Host
	 * @param {Number} config.url.port Port Number (not required)
	 * @param {String} config.url.path The path identifies the specific api resource
	 * @param {String} config.title Client title (used for error messages)
	 */
	constructor(config) {
		this._config = config;
		this._title = '';

		this._defaults = {
			pageSize: 'All',
			maxRecords: 10000000
		};
	}

	/**
	 * Client configuration
	 */
	get Config() {
		return this._config;
	}

	/**
	 * Default settings
	 */
	get Defaults() {
		return this._defaults;
	}

	/**
	 * Base URL for API enpoints
	 */
	get BaseUrl() {
		if (!this.Config || !this.Config.url) {
			return '';
		}

		if (!this._baseUrl) {
			this._baseUrl = utils.joinURL([
				this.Config.url.host + (this.Config.url.port ? ':' + this.Config.url.port : ''),
				this.Config.url.path
			]);
		}

		return this._baseUrl;
	}

	/**
	 * Client title
	 */
	get Title() {
		if (!this._title) {
			this._title = this.Config.title || ('The external server [' + this.BaseUrl + ']');
		}

		return this._title;
	}


	/**
	 * Private method for configuring request parameters
	 * @param {Object} data Request data
	 * @param {String} api Endpoint
	 * @param {Object} header HTTP Header
	 * @param {String} method HTTP Method: GET || POST || PUT || DELETE
	 * @returns {Object} Request options
	 */
	prepareOptions(data, api, header, method) {
		if (method !== 'GET' && method !== 'POST' && method !== 'PUT' && method !== 'DELETE') {
			throw new Error('Unrecognized method: ' + method);
		}

		let options = {
			baseUrl: this.BaseUrl,
			url: api[0] === '/' ? api : '/' + api,
			method: method,
			headers: header
		};

		options.headers['Accept'] = '*/*';
		if (!header.hasOwnProperty('Content-Type')) {
			options.headers['Content-Type'] = 'application/xml';
		}

		if (method === 'POST' || method === 'PUT') {
			options.headers['Content-Length'] = data.length;
			options.body = data;
		}

		if (options.headers['Content-Type'] === 'buffer') {
			options.headers['Content-Type'] = 'text/html';
			options.encoding = null;
		}
		this.prepareAuthorizationOptions(options);

		return options;
	}

	/**
	 * Private method for configuring Authorization options
	 * @param {Object} options Request options
	 */
	prepareAuthorizationOptions(options) {
		if (!this.Config || !this.Config.authorization) {
			return;
		}

		if (!this.Config.authorization.type || this.Config.authorization.type === 'Basic') {
			if (!this._token) {
				this._token = 'Basic ' + new Buffer(this.Config.authorization.username + ':' + this.Config.authorization.password).toString('base64');
			}
			options.headers['Authorization'] = this._token;
		}
	}

	/**
	 * Execute HTTP request
	 * @param {Object} obj Body
	 * @param {String} url Endpoint
	 * @param {Object} header Header
	 * @param {String} method Http method
	 * @returns {Receipt} receipt of execution
	 */
	async execRequest(obj, url, header, method) {
		let self = this;
		//postdata should be an xml
		//let postdata = obj ? utils.stringifyUnicode(obj) /*JSON.stringify(obj)*/ : '';
		let postdata = obj || '';

		let receipt = new Receipt(self.prepareOptions(postdata, url, header, method), this.Title);

		return new Promise(function (resolve, reject) {
			request(receipt.Request, function (err, response, body) {
				receipt.applyCB(err, response, body, function (err) {
					if (err) {
						return reject(err);
					}
					return resolve(receipt.ToRawObject());
				});
			});
		});

	}

	/**
	 * Execute HTTP GET request
	 * @param {String} api Endpoint
	 * @returns {Receipt} receipt of execution
	 */
	async execGetRequest(api) {
		return await this.execRequest(null, api, {}, 'GET');
	}

	/**
	 * Execute HTTP POST request
	 * @param {Object} obj Post object
	 * @param {String} api Endpoint
	 * @returns {Receipt} receipt of execution
	 */
	async execPostRequest(obj, api) {
		return await this.execRequest(obj, api, {}, 'POST');
	}

	/**
	 * Execute HTTP PUT request
	 * @param {Object} obj Put object
	 * @param {String} api Endpoint
	 * @returns {Receipt} receipt of execution
	 */
	async execPutRequest(obj, api) {
		return await this.execRequest(obj, api, {}, 'PUT');
	}

	/**
	 * Execute HTTP DELETE request
	 * @param {String} api Endpoint
	 * @returns {Receipt} receipt of execution
	 */
	async execDeleteRequest(api) {
		return await this.execRequest(null, api, {}, 'DELETE');
	}


	/**
	 * Retrieving a single record
	 * @param {String} api api for record
	 * @param {Number} id record key
	 * @returns {Receipt}
	 */
	async get(api, id) {
		if (!api) {
			throw new Error('Wrong api for a single record getter.');
		}

		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		return await this.execGetRequest(utils.joinURL([api, id]));
	}


	/**
	 * Retrieving a collection of objects
	 * @param {String} api api for a collection
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(api, options) {
		if (!api) {
			throw new Error('Wrong api for a collection getter.');
		}

		return await this.execGetRequest(utils.joinURL([api, utils.getListQuery(options, this.Defaults)]));
	}

	/**
	 * Retrieving a single record
	 * @param {String} api api for record
	 * @param {Number} id record key
	 * @returns {Receipt}
	 */
	async delete(api, id) {
		if (!api) {
			throw new Error('Wrong api for a single record deletion.');
		}

		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		return await this.execDeleteRequest(utils.joinURL([api, id]));
	}
};

Client.Utils = utils;