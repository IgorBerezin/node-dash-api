const Client = require('./client');

/**
 * Main Locations package
 * Represents functionality to request Locations API.
 * @extends {Client}
 * @return {Locations}
 */
const Locations = module.exports = class Locations extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'locations';
	}

	/**
	 * Remove a URI. Also remove all locations that were associated with the URI
	 * @param {Object} location Location object
	 * @param {String} location.Address1 Address Line 1
	 * @param {String} location.Address2 Optional Address Line 2
	 * @param {String} location.Community City or Town
	 * @param {String} location.Zip Postal or ZIP Code
	 * @param {String} location.State State/Province
	 * @returns {Receipt}
	 */
	async validate(location) {
		if (!location) {
			throw new Error('Location data is required.');
		}

		let xml =
			`<validateLocation>
				<location>
					<address1>${location.Address1}</address1>
					<address2>${location.Address2}</address2>
					<community>${location.Community}</community>
					<state>${location.State}</state>
					<postalcode>${location.Zip}</postalcode>
					<type>ADDRESS</type>
				</location>
			</validateLocation>`;

		return await super.execPostRequest(xml, 'validatelocation');
		/*
<ns1:validateLocationResponse xmlns:ns1=”http://dashcs.com/api/v1/emergency”>
  <Location>
    <address1>2040 Larimer St</address1>
    <community>Denver</community>
    <latitude>39.753439</latitude>
    <legacydata>
    <longitude>-104.991949</longitude>
    <plusfour>2015</plusfour>
    <postalcode>80205</postalcode>
    <state>CO</state>
    <status>
      <code>GEOCODED</code>
      <description>Location is geocoded</description>
    </status>
    <type>ADDRESS</type>
 </Location>
</ns1:validateLocationResponse>
		*/
	}

	/**
	 * Add location
	 * @param {Object} location Location object
	 * @param {String} location.Address1 Address Line 1
	 * @param {String} location.Address2 Optional Address Line 2
	 * @param {String} location.Community City or Town
	 * @param {String} location.Zip Postal or ZIP Code
	 * @param {String} location.State State/Province
	 * @returns {Receipt}
	 */
	async add(phone, location) {
		if (!phone) {
			throw new Error('Phone number data is required.');
		}

		if (!location) {
			throw new Error('Location data is required.');
		}

		let xml =
			`<addLocation>
				<uri>
					<uri>${phone.Number}</uri>
					<callername>${phone.Name}</callername>
				</uri>
				<location>
					<address1>${location.Address1}</address1>
					<address2>${location.Address2}</address2>
					<community>${location.Community}</community>
					<postalcode>${location.Zip}</postalcode>
					<plusfour></plusfour>
					<state>${location.State}</state>
					<type>ADDRESS</type>
				</location>
			</addLocation>`;




		return await super.execPostRequest(xml, 'addlocation');
		/*
<ns1:validateLocationResponse xmlns:ns1=”http://dashcs.com/api/v1/emergency”>
  <Location>
	<address1>2040 Larimer St</address1>
	<community>Denver</community>
	<latitude>39.753439</latitude>
	<legacydata>
	<longitude>-104.991949</longitude>
	<plusfour>2015</plusfour>
	<postalcode>80205</postalcode>
	<state>CO</state>
	<status>
	  <code>GEOCODED</code>
	  <description>Location is geocoded</description>
	</status>
	<type>ADDRESS</type>
 </Location>
</ns1:validateLocationResponse>
		*/
	}

	/**
	 * Remove a URI. Also remove all locations that were associated with the URI
	 * @param {Object} location Location object
	 * @param {String} location.Address1 Address Line 1
	 * @param {String} location.Address2 Optional Address Line 2
	 * @param {String} location.Community City or Town
	 * @param {String} location.Zip Postal or ZIP Code
	 * @param {String} location.State State/Province
	 * @returns {Receipt}
	 */
	async provision(id) {
		if (!id) {
			throw new Error('Location Id is required.');
		}

		let xml =
			`<provisionLocation>
				<locationid>${id}</locationid>
			</provisionLocation>`;

		return await super.execPostRequest(xml, 'provisionlocation');
		/*
<ns0:provisionLocationResponse xmlns:ns0="http://dashcs.com/api/v1/emergency">
    <LocationStatus>
        <code>PROVISIONED</code>
        <description>Location is currently provisioned for URI</description>
    </LocationStatus>
</ns0:provisionLocationResponse>
		*/
	}

	/**
	 * Remove a Location.
	 * @param {Number} id Location Id
	 * @returns {Receipt}
	 */
	async delete(id) {
		if (isNaN(id)) {
			throw new Error('Wrong location Id.');
		}

		let xml =
			`<removeLocation>
				<locationid>${id}</locationid>
			</removeLocation>`;
		return await super.execPostRequest(xml, 'removelocation');
		/*
		<ns0:removeLocationResponse xmlns:ns0="http://dashcs.com/api/v1/emergency">
			<LocationStatus>
				<code>REMOVED</code>
				<description>Location removed from the system</description>
			</LocationStatus>
		</ns0:removeLocationResponse>
		*/
	}
};