# node-dash-api

NodeJs Client library for for Bandwidth dash-api

## Supported Versions
This SDK stable for node versions 8 and above

## Release Notes
| Version | Notes |
|:--|:--|
| 0.0.1 | Initial |

## Install

Run

```
npm install node-dash-api
```

## Configuration
Bandwidth can provide clients with a “test system” for development and testing purposes.  Please contact Bandwidth with any questions about how to use the API or if you would like to have a test platform setup for your company.

Options:

  * `authorization` **Required**. Authorization block
    * `type` Auth type. [Basic]
    * `username` **Required**. Dash api login
    * `password` **Required**. Dash api password
  * `url` **Required**. Uniform Resource Locator's block
    * `host` **Required**. Dash api host
    * `port` Port Number.
    * `path` **Required**. The path identifies the specific api resource
  * `title` Client title

## Usage

### General principles
When you receive an API response, it will always contain the following information.

* `duration` Request duration
* `endpoint` API Endpoint
* `method` HTTP Method. ['GET', 'POST', 'PUT', 'DELETE']
* `headers` Headers
* `request` Body
* `error` Response Error (if any)
* `response` Either the Response object, or Error Message

Example:
```Javascript
const emergency = require('node-dash-api');
const config = {
	'authorization': {
		'type': 'Basic',
		'username': 'FakeUserName',
		'password': 'FakePassword'
	},
	'url': {
		'host': 'https://staging-service.dashcs.com',
		'path': '/dash-api/xml/emergencyprovisioning/v1'
	}
};

const uris = new emergency.URIs(config);

list = async function (context) {
	let result = await uris.list();
	console.log('Result: ', result);
	if (result.error) {
		throw result.error;
	}
	return result.response;
};

list().then();

```
Console:
```Javascript
Result:  { duration: 8041,
  endpoint: 'https://staging-service.dashcs.com/dash-api/xml/emergencyprovisioning/v1',
  method: 'GET',
  headers: 
   { Accept: '*/*',
     'Content-Type': 'application/json',
     'Content-Length': 6,
     Authorization: 'Basic xxx==' },
  request: '',
  error: null,
  response: null }
```

### API Objects

#### URIs
Get URIs:

```javascript
const uris = new emergency.Uris(config);
return await uris.list();

```

## Troubleshooting
- We're using Mocha as a test framework
- We can write our JavaScript in ES6, and compile it back to ES5 using Babel
- We can debug our tests using Node's built-in debugger, or using the Chrome DevTools
- We're measuring our test coverage using Istanbul

# Notes

Created as a module for another project. Some features still unavailable.

# License
MIT see LICENSE.

# Author
Igor Berezin <ig.v.berezin@gmail.com>