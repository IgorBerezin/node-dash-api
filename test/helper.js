const nock = require('nock');

const lib = require('../lib');
const test = require('./test');
const Client = lib.Client;

module.exports = {
	clientConfig: {
		'authorization': {
			'type': 'Basic',
			'username': 'FakeUserName',
			'password': 'FakePassword'
		},
		'url': {
			'host': 'https://staging-service.dashcs.com',
			'path': '/dash-api/xml/emergencyprovisioning/v1'
		}
	},

	createClient: function () {
		return new Client(module.exports.clientConfig);
	},

	nock: function () {
		return nock('https://staging-service.dashcs.com/dash-api/xml/emergencyprovisioning/v1');
	},

	test: test
};